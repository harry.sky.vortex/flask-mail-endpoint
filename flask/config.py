import os

class Config(object):
    #: Debug is turned off for security reasons.
    DEBUG = False
    #: Testing is turned off for security reasons.
    TESTING = False
    #: Secret key should be at least 32 characters long and as random as possible.
    SECRET_KEY = os.getenv('SECRET_KEY', 'not-very-secret-key')
    #: Mail sending is not suppressed, because it is the only thing we run this for.
    MAIL_SUPPRESS_SEND = False
    #: Default sender of mail, domain part should be set to domain configured on mail server side.
    MAIL_DEFAULT_SENDER = os.getenv('MAIL_DEFAULT_SENDER', 'Contact Form <contact@example.com>')
    #: By default - TLS is used to secure mail, any other value but 'true' will turn it off.
    MAIL_USE_TLS = os.getenv('MAIL_USE_TLS', 'true') == 'true'
    #: By default - it is host IP from Docker container perspective, but it can be any other IP or host.
    MAIL_SERVER = os.getenv('MAIL_SERVER', '172.17.0.1')
